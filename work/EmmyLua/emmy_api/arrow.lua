---@class arrow : entity
---
---When the [hero](https://doxygen.solarus-games.org/latest/lua_api_hero.html) uses a [hero:start_bow()](https://doxygen.solarus-games.org/latest/lua_api_hero.html#lua_api_hero_start_bow) bow, an arrow is created.
---
---This type of entity can only be created by the engine.
---
local m = {}

_G.arrow = m

return m