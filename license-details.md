# Licensing Details

Solarus uses exclusively open-source licenses.

## Solarus source code

The source code of Solarus is licensed under the terms of
the [GNU General Public License v3](https://www.gnu.org/copyleft/gpl.html).

**Authors:** Christopho and the Solarus team

- `include/**.h`
- `include/**.inl`
- `src/**.cpp`
- `tests/include/**.h`
- `tests/include/**.inl`
- `tests/src/**.cpp`

## Solarus Logos and Icons

Solarus logos and icons are licensed under
[Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0). Their source files can be found [here](https://gitlab.com/solarus-games/solarus-design).

**Author:** Olivier Cléro

- `images/solarus_logo.png`

## GUI icons

The following icons are licensed under [Creative Commons Attribution-ShareAlike 3.0](http://creativecommons.org/licenses/by-sa/3.0).

**Author:** [Yusuke Kamiyamane](http://p.yusukekamiyamane.com)

- `gui/resources/images/icon_add.png`
- `gui/resources/images/icon_open.png`
- `gui/resources/images/icon_remove.png`
- `gui/resources/images/icon_start.png`
- `gui/resources/images/icon_stop.png`

## Testing quest

Solarus provides a testing quest with a lot of data files, used for automated tests. It uses free assets from the [Solarus Free Resource Pack](https://github.com/solarus-games/solarus-free-resource-pack), whose licensing and authors are detailed [here](https://github.com/solarus-games/solarus-free-resource-pack/blob/dev/license.txt
).

**Authors:** Multiple authors

- `tests/testing_quest/*`
